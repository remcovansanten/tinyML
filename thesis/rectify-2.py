# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 15:14:37 2021

@author: ZeinstraCG
"""
#import sys
import os
import numpy as np
import glob
import cv2
from imutils import face_utils
import dlib


def thirdpoint(x1,y1,x2,y2):
	y1=-y1
	y2=-y2
	xm=0.5*(x1+x2)
	ym=0.5*(y1+y2)
	xd=0.5*(x2-x1)
	yd=0.5*(y2-y1)
	x3=xm+yd
	y3=ym-xd
	y3=-y3
	return (x3,y3)

def transformation(x1,y1,x2,y2,xd1,yd1,xd2,yd2):
	(x3,y3)=thirdpoint(x1,y1,x2,y2)
	(xd3,yd3)=thirdpoint(xd1,yd1,xd2,yd2)
	pts1 = np.float32([[x1,y1],[x2,y2],[x3,y3]])
	pts2 = np.float32([[xd1,yd1],[xd2,yd2],[xd3,yd3]])
	return cv2.getAffineTransform(pts1,pts2)

def createImage(img, affine, size):
    dst=cv2.warpAffine(img, affine, size)
    return dst    

p = "thesis/shape_predictor_68_face_landmarks.dat"
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(p)
size=(600,800)

l=glob.glob('thesis/Female/F*')

#counter=0

for item in l:
    #counter=counter+1
    #if counter>=11:
    #    break
    try:
        img=cv2.imread(item)
    except:
        print('Some error, continuing')
        continue
    rects = detector(img, 1)
    print(len(rects))
    if (len(rects)!=1):
        print("Either no face of more than one face")
        continue
    else:
        rect=rects[0]
        shape = predictor(img, rect)
        shape = face_utils.shape_to_np(shape)
    
        (x1,y1)=shape[36]
        (x2,y2)=shape[45]
    
        affine=transformation(x1, y1, x2, y2, 150, 300, 450, 300)
        warpedimg=createImage(img, affine, size)

        filename = os.path.split(item)[1]
        filename, file_extension = os.path.splitext(filename)
        newfilename=f'thesis/Female/{filename}_processed.png'
        cv2.imwrite(newfilename, warpedimg)