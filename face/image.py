# Source from : https://pythonprogramming.net/loading-images-python-opencv-tutorial/
import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('face/Face Database/001_02.jpg',cv2.IMREAD_GRAYSCALE)
#img = cv2.imread('watch.jpeg',cv2.IMREAD_GRAYSCALE)

plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
plt.plot([2000,3000,4000],[1000,2000,3000],'c', linewidth=5)
plt.show()